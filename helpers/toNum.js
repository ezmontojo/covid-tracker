// function for converting string data from the endpoint to numbers
export default function toNum(str) {

	// 23,143,197
	// Convert the string to an array to access array methods
	const stringArray = [...str]
	// ["2" , "3", "," , "1" , "4" , "3" ... ]

	// filter out the commas to the string
	const filteredArr = stringArray.filter(element => element !== ",");
	// ["2" ]

	// Reduce the filtered array to a single string without commas
	//  parseInt() converts the string into a number
	return parseInt(filteredArr.reduce((x, y) => x + y))
	// 1st iteration 
	// x = "", y = "2"
	// x = "2"
	// 2nd iteration 
	// x = "2", y = "3"
	// x = "23"
	// 3rd iteration
	// x = "23", y = "1"
	// x = "231"
	// final result = "23143197"

}