import React, { useState, useRef, useEffect } from 'react';
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
import toNum from '../../helpers/toNum';
import DoughnutChart from '../../components/DoughnutChart';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function Search( { data } ) {

	console.log(data);

	const countriesStats = data.countries_stat;

	console.log(countriesStats)

	const [targetCountry, setTargetCountry] = useState('');
	const [name, setName] = useState('');
	const [criticals, setCriticals] = useState(0);
	const [deaths, setDeaths] = useState(0);
	const [recoveries, setRecoveries] = useState(0);

	const mapContainerRef = useRef(null);

	// States for the mapbox properties
	const [latitude, setLatitude] = useState(0);
	const [longitude, setLongitude] = useState(0);
	const [zoom, setZoom] = useState(0);

	function search(e){

		e.preventDefault()

		const match = countriesStats.find(country => country.country_name.toLowerCase() === targetCountry.toLowerCase())

		console.log(match);

	if (match) {

		setName(match.country_name)
		setCriticals(toNum(match.serious_critical))
		setDeaths(toNum(match.deaths))
		setRecoveries(toNum(match.total_recovered))

		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${targetCountry}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setLongitude(data.features[0].center[0]);
			setLatitude(data.features[0].center[1]);
			setZoom(1);
	
		})
	
	} else {

		setName('');
		setCriticals(0);
		setDeaths(0);
		setRecoveries(0);	
	}
}

	useEffect(() => {

		// Instatiate a new Mapbox Map object
		const map = new mapboxgl.Map({
			// Set the container for the map
			container: mapContainerRef.current,
			// Style options for the map
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude, latitude],
			zoom: zoom
		})

		// Add navigation control (The +/- zoom buttons)
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')
		
		// Create a marker centered on the designated longitude and latitude
		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)

		// Clean up and release all resources associated with this map when component unmounts.
		// Not doing so, will result in an ever-increasing consumption 
		return () => map.remove()

	}, [latitude, longitude])



	return(
		<React.Fragment>
			<Form onSubmit={e => search(e)}>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control
						type="text"
						placeholder="Search for country"
						value={targetCountry}
						onChange={e => setTargetCountry(e.target.value)}
					/>
					<Form.Text className="text-muted">
						Get Covid-19 stats of searched for country.
					</Form.Text>
				</Form.Group>

				<Button variant="primary" type="submit">
					Submit
				</Button>
			</Form>

			{name !== ''
				?
					<React.Fragment>
						<h1>Country: {name}</h1>
						<DoughnutChart
							criticals={criticals}
							deaths={deaths}
							recoveries={recoveries}
					/>
					</React.Fragment>
				:
					<Alert variant="info" className="mt-4">
						Search for a country to visualize its data.
					</Alert>	

			}

			<Col xs={12} md={6} lg={12}>
				<div className="mapContainer" ref={mapContainerRef} />
			</Col>
		</React.Fragment>

	)
}

export async function getStaticProps() {

		const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
	    "method": "GET",
	    "headers": {
	      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
	      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
	    }
	  })
	  const data = await res.json()

	  return{
	  	props: {
	  		data
	  	}
	  }

}