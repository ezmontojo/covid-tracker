import React, { useState, useRef, useEffect } from 'react';
import{ Row, Col } from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
import toNum from '../../helpers/toNum';
import { Doughnut } from 'react-chartjs-2'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY


export default function TopCountries({topTenCountries}) {
console.log(topTenCountries)

let countries=[]
let cases=[]
topTenCountries.forEach(country =>{
countries.push(country.country_name)
cases.push(country.cases)
})

const mapContainerRef = useRef(null)


useEffect(()=>{
const map = new mapboxgl.Map({
// Set the container for the map
container: mapContainerRef.current,
//Style options for the map
style: 'mapbox://styles/mapbox/streets-v11',
center: [28.77,44.63],
zoom:1
})

map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

let marker
countries.forEach(country=>{
fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
.then(res => res.json())
.then(data =>{
console.log(data)

marker = new mapboxgl.Marker()
.setLngLat([data.features[0].center[0],data.features[0].center[1]])
.addTo(map)
})
})

return() => map.remove()
},[])





return(
<React.Fragment>
<h1>Top 10 Countries with Highest Covid-19 Cases</h1>

<Doughnut data={{
datasets: [{
data: cases,
backgroundColor:["#6a686a", "#c2d65f", "#aadbea", "#de2d1d", "#c186b8", "#546e94", "#cbcdab", "#371223", "#341120", "#1e3238", "#2d5536"]
}],
labels:countries
}} redraw={false}/>

<Col xs={12} md={6} lg={12}>
<div className = "mapContainer" ref={ mapContainerRef }/>
</Col>
</React.Fragment>
)
}


export async function getStaticProps(){

const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
   "method": "GET",
   "headers": {
    "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
    "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
   }
})
const data = await res.json()

  const countriesStats = data.countries_stat;

let countriesCases=[]
 countriesStats.forEach(country =>{
countriesCases.push(
{
country_name: country.country_name,
cases: toNum(country.cases)
}
)
})

function higherCase( a, b ) {
if ( a.cases > b.cases ){
return -1;
}else if ( a.cases < b.cases ){
return 1;
}else{
return 0;
}
}

countriesCases.sort( higherCase );
let topTenCountries = countriesCases.slice(0,10)

return {
props:{
topTenCountries
}
}
}
