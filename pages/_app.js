import React from 'react';
import { Container } from 'react-bootstrap';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import NavBar from '../components/NavBar';

function MyApp({ Component, pageProps }) {
		  return (
		  	<React.Fragment>
			  	<NavBar />
			  	<Container>
			  		<Component {...pageProps} />
			  	</Container>
			</React.Fragment>
		)	
	}
		

export default MyApp
