import React, { useState, useRef, useEffect } from 'react';
import{ Form, Button,Alert, Row, Col } from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY



export default function GlobalMap({country}){

const mapContainerRef = useRef(null)

const [latitude,setLatitude] = useState(0);
const [longitude,setLonghitude] = useState(0);
const [zoom,setZoom] = useState(0);

fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${country}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
.then(res => res.json())
.then(data =>{
console.log(data)
setLonghitude(data.features[0].center[0])
setLatitude(data.features[0].center[1])
setZoom(1)
})

useEffect(()=>{

//Instatiate a new Mapbox Map object
const map = new mapboxgl.Map({
container: mapContainerRef.current,
style: 'mapbox://styles/mapbox/streets-v11',
center: [longitude,latitude],
zoom:zoom
})

map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

const marker = new mapboxgl.Marker()
.setLngLat([longitude,latitude])
.addTo(map)

return() => map.remove()

},[latitude,longitude])

return (
<Col xs={12} md={6} lg={12}>
<div className = "mapContainer" ref={ mapContainerRef }/>
</Col>

)

}